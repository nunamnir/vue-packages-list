import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    database: [],
    modals: {
      modalIsOpen: false,
      modalPayload: {}
    }
  },
  mutations: {
    addDataFromApi(state){
      axios.get('https://data.jsdelivr.com/v1/stats/packages').then(resp => {
        state.database = resp.data;
      });
    },
    setModalPayload: (state, arg) => {
      state.modals.modalPayload = arg;
    },
  },
  actions: {
    addDataFromApi: (context) => {
      context.commit('addDataFromApi');
    },
    setModalPayload: (context, arg) => {
      context.commit('setModalPayload', arg);
    },
  },
  getters: {
    getDatabase(state) {
      return state.database;
    },
    getModalPayload(state) {
      return state.modals.modalPayload;
    },
  }
})
